﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

    public static PoolManager instance;

    [SerializeField]
    private string[] m_objectNames;
    [SerializeField]
    private GameObject[] m_pooledObjects;
    [SerializeField]
    private int[] m_pooledAmounts;

    private Hashtable mainPool = new Hashtable();

    private List<GameObject> tempList;

    void Awake()
    {
        instance = this;
    }

	void Start ()
    {
        tempList = new List<GameObject>();

        for (int i = 0; i < m_objectNames.Length; i++)
        {
            List<GameObject> objList = new List<GameObject>();

            for (int j = 0; j < m_pooledAmounts[i]; j++)
            {
                GameObject obj = (GameObject)Instantiate(m_pooledObjects[i]);
                obj.transform.parent = transform;
                obj.SetActive(false);
                objList.Add(obj);
            }

            mainPool.Add(m_objectNames[i], objList);
        }
	}
	
	public GameObject GetPooledObject(string name)
    {
        if (mainPool.ContainsKey(name))
        {
            tempList = mainPool[name] as List<GameObject>;

            for (int i = 0; i < tempList.Count; i++)
            {
                if (tempList[i] != null)
                {
                    if (!tempList[i].activeInHierarchy)
                    {
                        return tempList[i];
                    }
                }
            }
            return null;
        }

        return null;
    }

    public void ResetPool()
    {
        for (int i = 0; i < tempList.Count; i++)
        {
            if (tempList[i] != null)
            {
                if (tempList[i].activeInHierarchy)
                {
                    tempList[i].SetActive(false);
                }
            }
        }
    }
}
