﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ThrowNewspaper : MonoBehaviour {

    [SerializeField]
    private Transform m_playerView;

    [SerializeField]
    private Text m_remainingPapersText;

    private bool m_canThrow;
    [SerializeField]
    private float m_throwDelay;

    private int m_paperCount;
    private int m_remainingPapers;

    void Start()
    {
        m_canThrow = true;
        if (GameManager.instance != null)
        {
            m_paperCount = GameManager.instance.m_paperCount;
            m_remainingPapers = m_paperCount;
        }
    }

    void Update()
    {
        if (GameManager.instance != null)
        {
            if (m_paperCount != GameManager.instance.m_paperCount)
            {
                m_paperCount = GameManager.instance.m_paperCount;
                m_remainingPapers = m_paperCount;
            }

            m_remainingPapersText.text = m_remainingPapers.ToString();

            if (Input.GetMouseButtonDown(0))
            {
                if (m_canThrow && m_remainingPapers > 0)
                {
                    ThrowPaper();
                }
            }
        }
        //if the game is in free roam mode (IE the menu)
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                ThrowPaper();
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SoundManager.instance.RingBell();
        }
    }

    private void ThrowPaper()
    {
        m_canThrow = false;
        Invoke("ThrowReset", m_throwDelay);

        m_remainingPapers--;

        GameObject newPaper = PoolManager.instance.GetPooledObject("Paper");
        if (newPaper != null)
        {
            newPaper.transform.position = m_playerView.transform.position;
            newPaper.transform.rotation = m_playerView.transform.rotation;
            newPaper.SetActive(true);
        }
    }

    private void ThrowReset()
    {
        m_canThrow = true;
    }
}
