﻿using UnityEngine;
using System.Collections;

public class SpinningHUDImage : MonoBehaviour {

    private bool m_rotatingClockwise;
    private bool m_spinning;

    void Start()
    {
        m_rotatingClockwise = true;

    }

    void OnEnable()
    {
        m_spinning = true;
    }

    void OnDisable()
    {
        m_spinning = false;
    }
	
	void Update ()
    {
        if (m_spinning)
        {
            if (gameObject.transform.rotation.eulerAngles.z < 360 && gameObject.transform.rotation.eulerAngles.z > 60)
            {
                m_rotatingClockwise = !m_rotatingClockwise;
            }

            if (m_rotatingClockwise)
            {
                gameObject.transform.Rotate(new Vector3(0, 0, .5f));
            }
            else
            {
                gameObject.transform.Rotate(new Vector3(0, 0, -.5f));
            }
        }
	}
}
