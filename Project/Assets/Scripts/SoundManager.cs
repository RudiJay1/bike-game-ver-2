﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;

    [SerializeField]
    private AudioSource[] sound = new AudioSource[5];
    [SerializeField]
    private AudioClip[] dialogueClip = new AudioClip[11];

    private bool m_isTalking;

    void Awake()
    {
        instance = this;
    }

	void Start ()
    {
        m_isTalking = false;

        PlayMusic();
	}
	
	public void PlayDialogue(int clipNo)
    { 
        if (!m_isTalking)
        {
            sound[3].clip = dialogueClip[clipNo];
            sound[3].Play();
            m_isTalking = true;
            Invoke("ResetTalking", dialogueClip[clipNo].length);
        }
        else
        {
            StartCoroutine("QueueClip", clipNo);
        }
    }

    private void ResetTalking()
    {
        m_isTalking = false;
    }

    IEnumerator QueueClip(int clipNo)
    {
        yield return new WaitForSeconds(sound[0].clip.length);
        PlayDialogue(clipNo);
    }

    private void PlayMusic()
    {
        sound[1].Play();
    }

    public void RingBell()
    {
        sound[2].Play();
    }

    public void StartCycling()
    {
        sound[0].Play();
    }

    public void StopCycling()
    {
        sound[0].Stop();
    }
}
