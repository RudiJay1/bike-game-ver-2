﻿using UnityEngine;
using System.Collections;

public class Mailbox : MonoBehaviour {

    [SerializeField]
    private GameObject m_flag, m_door;
    private ParticleSystem ps;

    private Renderer houseColor;
    private Color[] subscriberColors = new Color[3];
    private Color nonSubscriberColor;

    private bool m_isSubscriber;
    public bool m_isFilled;

    void Start()
    {
        ps = gameObject.transform.Find("Particle System").GetComponent<ParticleSystem>();
        houseColor = gameObject.transform.Find("House").Find("Walls").GetComponent<Renderer>();

        subscriberColors[0] = new Color(0.59f, 0.82f, 0.84f);
        subscriberColors[1] = new Color(0.8f, 0.8f, 0.8f);
        subscriberColors[2] = new Color(1f, 0.96f, 0.64f);
        nonSubscriberColor = new Color(0.32f, 0f, 0f);

        m_isFilled = false;
    }

    public void EnableHouse()
    {
        transform.Find("House").gameObject.SetActive(true);
        if (m_isSubscriber)
        {
            houseColor.material.color = subscriberColors[Random.Range(0, 2)];
        }
        else
        {
            houseColor.material.color = nonSubscriberColor;
        }
    }

    public void CleanUp()
    {
        EmptyMailbox();
        transform.Find("House").gameObject.SetActive(false);
    }

    public void DepositPaper()
    {
        m_isFilled = true;

        m_flag.transform.rotation = Quaternion.Euler(106.197f, 0, 0);
        m_door.transform.rotation = Quaternion.Euler(0, 0, 0);
        ps.Play();

        if (m_isSubscriber)
        {
            SoundManager.instance.PlayDialogue(Random.Range(0, 10));
            GameManager.instance.AddScore(700);
        }
        else
        {
            SoundManager.instance.PlayDialogue(Random.Range(11, 17));
        }
    }

    public void EmptyMailbox()
    {
        m_isFilled = false;

        m_flag.transform.rotation = Quaternion.Euler(17.214f, 0, 0);
        m_door.transform.rotation = Quaternion.Euler(106.547f, 0, 0);
    }

    public void SetSubscriber(bool isSubscriber)
    {
        m_isSubscriber = isSubscriber;
    }

    public bool GetFilled()
    {
        return m_isFilled;
    }
}
