﻿using UnityEngine;
using System.Collections;

public class Newspaper : MonoBehaviour {

    private Rigidbody m_rb;

    [SerializeField]
    private float m_throwSpeed;

    private AudioSource audio;
    [SerializeField]
    private AudioClip[] sound = new AudioClip[2];

    void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

	void OnEnable ()
    {
        m_rb.velocity = gameObject.transform.forward * m_throwSpeed;

        audio.clip = sound[0];
        audio.Play();

        Invoke("SelfDestruct", 2);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Mailbox")
        {
            Mailbox box = collision.collider.gameObject.GetComponent<Mailbox>();

            if (!box.GetFilled())
            {
                box.DepositPaper();

                audio.clip = sound[1];
                audio.Play();

                Invoke("SelfDestruct", audio.clip.length);
            }
            else
            {
                Hit();
            }
        }
        else
        {
            Hit();
        }
    }

    private void Hit()
    {
        audio.clip = sound[2];
        audio.Play();

        Invoke("SelfDestruct", audio.clip.length);
    }

    private void SelfDestruct()
    {
        gameObject.SetActive(false);
    }
}
