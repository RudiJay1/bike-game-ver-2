﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField]
    private Mailbox[] mailboxes = new Mailbox[16];
    private List<Mailbox> subscribers = new List<Mailbox>();
    private List<Mailbox> notSubscribed = new List<Mailbox>();

    [SerializeField]
    private GameObject m_player, m_skyCam;

    [SerializeField]
    private Canvas m_UICanvas;
    private GameObject m_dayScreen;
    private Text m_dayText;
    private GameObject m_subscriptionScreen;
    private Text m_subscriptionText;
    private Text m_roadText;
    private GameObject m_gameplayUI;
    private GameObject m_levelEndScreen;
    private Text m_levelEndText;
    private Text m_endScoreText;
    private Text m_newSubscribersText;
    private GameObject m_leaderboardScreen;
    private Text m_leaderboard;
    private Text m_resultText;

    private bool m_isGameplay;
    private bool m_levelEnded;

    private List<int> m_highScoreArray;

    [SerializeField]
    private Text m_timerText;
    private float m_timeLimit;
    private float m_lastTime;

    [SerializeField]
    private int m_levelNo;
    private int m_subscriberAmount;
    private int m_score;
    private Text m_scoreText;

    public int m_paperCount;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        m_player.SetActive(false);
        m_skyCam.SetActive(true);

        m_dayScreen = m_UICanvas.transform.Find("DayScreen").gameObject;
        m_dayText = m_dayScreen.transform.Find("DayText").GetComponent<Text>();

        m_subscriptionScreen = m_UICanvas.transform.Find("SubscriptionScreen").gameObject;
        m_subscriptionText = m_subscriptionScreen.transform.Find("SubscriptionText").GetComponent<Text>();
        m_roadText = m_subscriptionScreen.transform.Find("RoadText").GetComponent<Text>();

        m_gameplayUI = m_UICanvas.transform.Find("Gameplay").gameObject;
        m_timerText = m_gameplayUI.transform.Find("Timer").GetComponent<Text>();
        m_scoreText = m_gameplayUI.transform.Find("Score").GetComponent<Text>();

        m_levelEndScreen = m_UICanvas.transform.Find("EndScreen").gameObject;
        m_levelEndText = m_levelEndScreen.transform.Find("LevelEndText").GetComponent<Text>();
        m_endScoreText = m_levelEndScreen.transform.Find("ScoreText").GetComponent<Text>();
        m_newSubscribersText = m_levelEndScreen.transform.Find("SubscriberText").GetComponent<Text>();

        m_leaderboardScreen = m_UICanvas.transform.Find("Leaderboard").gameObject;
        m_leaderboard = m_leaderboardScreen.transform.Find("Leaderboard").GetComponent<Text>();
        m_resultText = m_leaderboardScreen.transform.Find("Leaderboard").GetComponent<Text>();

        m_highScoreArray = new List<int>();
        m_highScoreArray.Add(70000);
        m_highScoreArray.Add(68000);
        m_highScoreArray.Add(65000);
        m_highScoreArray.Add(60000);
        m_highScoreArray.Add(55000);
        m_highScoreArray.Add(50000);
        m_highScoreArray.Add(45000);
        m_highScoreArray.Add(40000);
        m_highScoreArray.Add(35000);    
        m_highScoreArray.Add(30000);

        m_levelNo = 1;
        m_subscriberAmount = 8;
        m_score = 0;

        m_isGameplay = false;

        m_lastTime = 0;

        InstantiateLevel();
    }

    void Update()
    {
        if (m_isGameplay)
        {
            m_scoreText.text = m_score.ToString("00000");

            if (m_timeLimit > 0)
            {
                float minutes = Mathf.Floor(m_timeLimit / 60);
                float seconds = Mathf.RoundToInt(m_timeLimit % 60);
                m_timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00");

                if (m_timeLimit <= 10)
                {
                    m_timerText.color = Color.red;
                }
                else
                {
                    m_timerText.color = Color.white;
                }

                m_timeLimit -= (Time.time - m_lastTime);
            }
            else
            {
                if (!m_levelEnded)
                {
                    Debug.Log("Ending level");
                    StartCoroutine(EndLevel());
                }
                m_levelEnded = true;
            }

            m_lastTime = Time.time;
        }
    }

    //sets up the time the player has and the mailboxes the player must deliver to per level
    private void InstantiateLevel()
    {
        Debug.Log(m_levelNo);
        subscribers.Clear();
        notSubscribed.Clear();
        
        //fill up nonsubscribers list
        for (int i = 0; i < mailboxes.Length; i++ )
        {
            notSubscribed.Add(mailboxes[i]);
        }

        //assign every subscriber a random unsubscribed mailbox
        for (int i = 0; i < m_subscriberAmount; i++)
        {
            int subscriberIndex = Random.Range(0, notSubscribed.Count);
            subscribers.Add(notSubscribed[subscriberIndex]);
            subscribers[i].SetSubscriber(true);

            notSubscribed.RemoveAt(subscriberIndex);
        }

        //make sure all non subscribers are reset
        for (int i = 0; i < notSubscribed.Count; i++)
        {
            notSubscribed[i].SetSubscriber(false);
        }

        StartCoroutine(StartLevel());
    }

    private IEnumerator StartLevel()
    {
        //show the day screen
        m_dayText.text = GetDay();
        m_dayScreen.SetActive(true);
        yield return new WaitForSeconds(3f);
        m_dayScreen.SetActive(false);

        //show the subscriber screen, first showing subscribers
        m_subscriptionText.text = "Your Customers";
        m_roadText.text = GetRoadName();
        m_subscriptionScreen.SetActive(true);

        for (int i = 0; i < subscribers.Count; i++ )
        {
            subscribers[i].EnableHouse();          
        }
        yield return new WaitForSeconds(3f);

        //then showing everything else
        m_subscriptionText.text = "Non-Customers";
        for (int i = 0; i < notSubscribed.Count; i++)
        {
            notSubscribed[i].EnableHouse();
        }
        yield return new WaitForSeconds(3f);
        m_subscriptionScreen.SetActive(false);

        //set time limit for level
        m_timeLimit = GetTimeLimit();
        //set number of papers for level
        m_paperCount = GetPaperCount();

        //start gameplay
        m_skyCam.SetActive(false);
        m_gameplayUI.SetActive(true);
        m_player.SetActive(true);

        m_isGameplay = true;
        m_levelEnded = false;
    }

    private IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(1f);
        m_isGameplay = false;

        m_gameplayUI.SetActive(false);
        m_player.SetActive(false);
        m_skyCam.SetActive(true);

        m_levelEndScreen.SetActive(true);

        yield return new WaitForSeconds(1f);
        m_levelEndText.text = "Delivery Report: " + GetDay();
        m_levelEndText.enabled = true;

        yield return new WaitForSeconds(1f);
        m_endScoreText.text = "Score: " + m_score.ToString("000000");
        m_endScoreText.enabled = true;

        yield return new WaitForSeconds(1f);
        m_newSubscribersText.text = GetNewSubscribers();
        m_newSubscribersText.enabled = true;

        yield return new WaitForSeconds(3f);
        CleanUpLevel();

        //start next level
        Debug.Log("starting new level");
        if (m_levelNo < 7)
        {
            m_levelNo++;        
            InstantiateLevel();
        }
        else
        {
            StartCoroutine(ShowLeaderboard());
        }
    }

    private IEnumerator ShowLeaderboard()
    {
        for (int i = 0; i < m_highScoreArray.Count; i++)
        {
            if (m_score >= m_highScoreArray[i] && m_highScoreArray[i] != 0)
            {
                m_highScoreArray.Insert(i, m_score);
                m_resultText.text = "Congratulations, you got a high score!";
                break;
            }
            else
            {
                m_resultText.text = "You're at the bottom of the leaderboard! You suck!";
            }
        }

        m_leaderboard.text = m_highScoreArray[0].ToString() + "\n" + m_highScoreArray[1].ToString() + "\n" + m_highScoreArray[2].ToString() +
            "\n" + m_highScoreArray[3].ToString() + "\n" + m_highScoreArray[4].ToString() + "\n" + m_highScoreArray[5].ToString() + "\n" + m_highScoreArray[6].ToString() +
            "\n" + m_highScoreArray[7].ToString() + "\n" + m_highScoreArray[8].ToString() + "\n" + m_highScoreArray[9].ToString();

        m_leaderboardScreen.SetActive(true);
        yield return new WaitForSeconds(5);

        Application.LoadLevel(0);
    }

    private void CleanUpLevel()
    {
        //UI
        m_levelEndText.enabled = false;
        m_endScoreText.enabled = false;
        m_newSubscribersText.enabled = false;
        m_levelEndScreen.SetActive(false);

        //set mailboxes to unfilled and non-subscribed
        for (int i = 0; i < mailboxes.Length; i++)
        {
            mailboxes[i].SetSubscriber(false);
            mailboxes[i].CleanUp();
        }
    }

    private string GetNewSubscribers()
    {
        string text;
        //check to see if any subscribers didn't get their paper and unsubscribe them
        bool perfectDelivery = true;
        int subscribersCancelled = 0;
        for (int i = 0; i < subscribers.Count; i++)
        {
            if (!subscribers[i].GetFilled())
            {
                //set their house to red
                subscribers[i].SetSubscriber(false);
                subscribers[i].EnableHouse();
                
                m_subscriberAmount--;

                subscribersCancelled++;
                perfectDelivery = false;
            }
        }
        //if no papers were undelivered, gain a new subscriber
        if (perfectDelivery)
        {
            m_subscriberAmount++;
            text = "A New Subscriber!\nPerfect Delivery!!";
        }
        else
        {
            text = subscribersCancelled + " Cancelled Subscriptions";
        }
        return text;
    }

    private string GetDay()
    {
        string day;
        switch (m_levelNo)
        {
            case 1:
                day = "Monday";
                break;
            case 2:
                day = "Tuesday";
                break;
            case 3:
                day = "Wednesday";
                break;
            case 4:
                day = "Thursday";
                break;
            case 5:
                day = "Friday";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Sunday";
                break;
            default:
                day = "Day";
                break;
        }
        return day;
    }

    private float GetTimeLimit()
    {
        float timeLimit;

        timeLimit = (m_subscriberAmount * 10);

        return timeLimit;
    }

    private int GetPaperCount()
    {
        int paperCount;

        paperCount = 7 + m_subscriberAmount;

        return paperCount;
    }

    private string GetRoadName()
    {
        string[] firstPart = {"First", "Second", "Third", "Fourth", "Fitfh", "Sixth", "Seventh"};
        string[] secondPart = { "Street", "Avenue", "Terrace"};

        string roadName = firstPart[m_levelNo - 1] + " " + secondPart[Random.Range(0, secondPart.Length - 1)];

        return roadName;
    }

    public void AddScore(int points)
    {
        m_score += points;
    }
}
