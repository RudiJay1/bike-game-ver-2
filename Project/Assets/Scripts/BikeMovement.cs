﻿using UnityEngine;
using System.Collections;

public class BikeMovement : MonoBehaviour {

    [SerializeField]
    private Rigidbody bikeBody;
    [SerializeField]
    private GameObject bikeFront;
    [SerializeField]
    private GameObject frontWheel, backWheel;

    private float m_bikeAcceleration;
    private float m_bikeRotation = 0f, m_handlebarRotation = 0f;
    [SerializeField]
    private float m_bikeSpeed = 5.0f, m_bikeMaxSpeed = 50f, m_bikeRotationSpeed = 5f, m_handlebarRotationSpeed = 8f;

    [SerializeField]
    private float m_jumpForce = 200f;

    private float m_distToGround, m_distToSide;

    private bool m_isCycling;

    
    
    void Start()
    {
        //get the height from the ground
        m_distToGround = (backWheel.GetComponent<SphereCollider>().radius) + 0.2f;
        m_distToSide = (bikeBody.GetComponent<BoxCollider>().size.x / 2) + 0.05f;
    }

    void OnEnable()
    {
        bikeBody.position = new Vector3(3, .74f, 19.97f);
        bikeBody.rotation = Quaternion.Euler(-90, 0, 0);
    }

	void Update ()
    {
        InputHandler();
    }

    private void InputHandler()
    {
        Debug.DrawRay(backWheel.transform.position, -backWheel.transform.forward * m_distToGround);

        //bike rotation
        m_bikeRotation = Input.GetAxis("Horizontal") * m_bikeRotationSpeed;
        bikeBody.transform.Rotate(new Vector3(0, 0, m_bikeRotation));

        //handlebar rotation
        m_handlebarRotation = Input.GetAxis("Horizontal") * m_handlebarRotationSpeed;
        bikeFront.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Clamp(m_handlebarRotation, -25f, 25f));

        if (IsGrounded())
        {
            //movement
            m_bikeAcceleration = Input.GetAxis("Vertical") * m_bikeSpeed * Time.deltaTime;
            bikeBody.velocity += bikeBody.transform.right * m_bikeAcceleration;

            //limiting speed
            if (bikeBody.velocity.magnitude > m_bikeMaxSpeed)
            {
                bikeBody.velocity = bikeBody.velocity.normalized * m_bikeMaxSpeed;
            }
            else if (bikeBody.velocity.magnitude < -m_bikeMaxSpeed)
            {
                bikeBody.velocity = bikeBody.velocity.normalized * -m_bikeMaxSpeed;
            }

            //leaning left/right
            if (bikeBody.velocity.magnitude > 0)
            {
                bikeBody.transform.Rotate(new Vector3(Mathf.Clamp(-m_handlebarRotation/2f, -1, 1), 0, 0));
            }
            if (m_handlebarRotation == 0)
            {
                bikeBody.transform.rotation = Quaternion.RotateTowards(bikeBody.transform.rotation, 
                    Quaternion.Euler(-90, bikeBody.transform.rotation.eulerAngles.y, bikeBody.transform.rotation.eulerAngles.z), 1f);
            }

            //jumping
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
        }

        //play cycling audio
        if (bikeBody.velocity.magnitude > 0.1f || bikeBody.velocity.magnitude < -0.1f)
        {
            if (!m_isCycling)
            {
                m_isCycling = true;
                SoundManager.instance.StartCycling();
            }
        }
        else
        {
            if (m_isCycling)
            {
                m_isCycling = false;
                SoundManager.instance.StopCycling();
            }
        }

        //getting back up
        if (Input.GetKeyDown(KeyCode.R))
        {
            GetUp();
        }
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(backWheel.transform.position, -backWheel.transform.forward, m_distToGround);
    }

    private bool IsFallen()
    {
        bool fallen = false;

        //if the bike isn't standing upright, and is also against the ground;
        if (!IsGrounded() && ((Physics.Raycast(bikeBody.transform.position, new Vector3(0, 0, 1), m_distToSide)
             || Physics.Raycast(bikeBody.transform.position, new Vector3(0, 0, -1), m_distToSide))))
        {
            if (bikeBody.velocity.magnitude == 0)
            {
                fallen = true;
            }
        }

        return fallen;
    }

    private void Jump()
    {
        bikeBody.AddForce(new Vector3(0, 1, 0) * m_jumpForce);
    }

    private void GetUp()
    {
        bikeBody.transform.position += new Vector3(0, 1, 0);

        bikeBody.transform.rotation = Quaternion.Euler(-90, bikeBody.transform.rotation.eulerAngles.y, bikeBody.transform.eulerAngles.z);

        bikeBody.velocity = Vector3.zero;
    }

    
}
