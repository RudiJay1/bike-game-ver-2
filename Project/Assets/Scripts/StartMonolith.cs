﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMonolith : MonoBehaviour {

    [SerializeField]
    private Text instructions;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Paper")
        {
            //make the monolith fall over
            gameObject.GetComponent<Rigidbody>().isKinematic = false;

            instructions.text = "Loading game...";

            StartCoroutine(LoadGame());
        }
    }

    private IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(2f);

        Application.LoadLevel(1);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
